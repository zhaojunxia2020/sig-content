# September.27, 2022 at 10:00am GMT+8

## Agenda
|时间|议题|发言人|
|--|--|--|
|10:00-10:15|Mips编译工具和框架适配进展同步|Lain 袁祥仁|
|10:15-10:25|L1系统功能增强需求沟通|袁祥仁 黄首西|
|10:25-10:35|系统三方组件适配|Lain 黄首西 刘佳科|

## Attendees
- [@hongtao6573](https://gitee.com/hongtao6573)
- [@cip_syq](yunqiang.su@oss.cipunited.com)
- [@Lain]()
- [@wicom](https://gitee.com/wicom)
- [@huanghuijin](https://gitee.com/huanghuijin)
- [@huangsox](https://gitee.com/huangsox)
- [@liujk000](https://gitee.com/liujk000)


## Notes

#### 议题一、Mips编译工具和框架适配进展同步

**结论**
- 当前init启动正常，10月中旬前完成参数调优。

#### 议题二、L1系统功能增强需求沟通

**结论**
- 收集需求详细信息，在下次例会前联系相关子系统负责人确认。（刘佳科）


#### 议题三、系统三方组件适配进展同步

**结论**
- boringssl仓库已经退休，暂停适配。
- flutter适配再mips平台下难度很大且收益不明显，暂停适配。
- libffi,libpng,libunwind已支持mips平台，需要在编译框架适配完成后测试性能。
- openssl正在安排人力投入。


## Action items
