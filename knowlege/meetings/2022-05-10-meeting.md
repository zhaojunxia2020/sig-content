# May 10, 2022 at 15:00pm GMT+8

## Agenda
+ 遗留问题进展
+ 分布式数据管理专题课程评审
+ OpenHarmony生态专刊（小册子）开发套件信息看护
+ 软通动力标准系统开发样例上官网

## Attendees
- [zeeman_wang](https://gitee.com/zeeman_wang)
- [zhanglunet](https://gitee.com/zhanglunet)
- [Cruise2019](https://gitee.com/Cruise2019)
- [madixin](https://gitee.com/madixin)
- [深开鸿-guoyuefeng](guoyuefeng@kaihongdigi.com)
- [小熊派-peijia](peijia@holdiot.com)
- [OpenBlock-dutianwei](https://gitee.com/duzc2)
- [沈春萍]()
- [涂文星]()
- [袁博文]()
- [薛澎]()
- [董龙涛]()

## Notes

#### 议题一、遗留问题进展（张前福）

**知识体系技术评审团**

**进展**

+ 极客秀征文评审，目前已经反馈专家6份，其余专家反馈意见待跟踪

#### 议题二、分布式数据管理专题课程评审（董龙涛）

**进展**

- 课程主题：分布式数据管理专题课程评审（录制）

- 课程形式：录播

- 开发平台：OpenHarmony 3.1 Release

- 课程目标： 通过讲解分布式数据管理整体及模块功能及接口介绍，并结合开发实例，让开发者“会用、善用”分布式数据管理子系统。

- 课程设置：

  1 OpenHarmony分布式数据管理之整体介绍
  2 OpenHarmony分布式数据管理之分布式KV数据库
  3 OpenHarmony分布式数据管理之数据同步
  4 OpenHarmony分布式数据管理之关系型数据库
  5 OpenHarmony分布式数据管理之数据对象
  6 OpenHarmony分布式数据管理之轻量级偏好数据库
  7 OpenHarmony分布式数据管理之数据共享

**结论**

+ 同意该期专题课程设置
+ 课程需要显示标识使用的版本和平台

#### 议题三、OpenHarmony生态专刊（小册子）开发套件信息看护（薛澎）

**进展**

​	关于OH生态专刊开发套件栏目的内容素材需求，总编室小组按营销组会议结论提案由知识体系组负责套件信息列表的汇总及看护

**结论**

知识体系组主要看护职能偏软件方向（即：开发样例）。开发套件信息资料看护的具体责任主体的确定，会后会同治文老师、欧SIR等相关负责人拉会沟通商议。

#### 议题四、软通动力标准系统开发样例上官网（袁博文）

**进展**

+ 申报两个标准系统级开发样例，包括分布式画板和智能门禁人脸识别(JS)样例，希望该样例能够在官网呈现。当前样例效果已经ready,托管在自己企业仓库。

 **结论**

+ 会议通过该批样例（2个）上官网的申请。
+ 相关样例补充好说明文档，提交到知识体系仓库

## Action items
无
