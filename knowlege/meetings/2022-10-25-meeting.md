# Oct 25, 2022 at 15:00pm GMT+8

## Agenda
+ 遗留问题汇总
+ 知识体系联合立项进展
+ Codelabs开发样例上官网评审  
## Attendees
- [zeeman_wang](https://gitee.com/zeeman_wang)
- [张路](zhanglu@iscas.ac.cn)
- [madixin](https://gitee.com/madixin)
- [Cruise2019](https://gitee.com/Cruise2019)
- [深开鸿-guoyuefeng](guoyuefeng@kaihongdigi.com)
- [华为-张云虎]()
- [润和-连志安]()  
## Notes
#### 议题一、知识体系联合立项进展（张前福）  
**进展**  
关于HDC展示的开发样例，在截止日前收到展品24件，覆盖了轻量系统、小型系统、标准系统，涉及车载语音、智能家居、高清播放、安防监控等领域。  
#### 议题二、Codelabs开发样例上官网评审（张云虎）  
**进展**   
完成了1个Codelabs样例：  
+ HDF音频驱动模型应用（C）: RK3568, 3.2 Beta, 通过HDI接口实现音频放音流程、放音输出设备切换  
**结论**  
同意该批次1个codelabs上官网的申请  


[本次会议录播视频](https://pan.baidu.com/s/1_eoh5vpbDZiG6gsoqSQaHg?pwd=wzr4)  

## Action items


