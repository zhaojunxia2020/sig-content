# August 4, 2021 at 8:00pm GMT+8

## Agenda/议程表
- 讲解linkboy对OpenHarmony对接的技术架构 （王强）
- 目前已支持的OpenHarmony功能 （王强）
- 目前遇到的技术问题（创建wifi热点后手机连接到hi3861开发板提示网络已拒绝） （王强）
- 向linkboy用户群推介OpenHarmony思路探讨，开展青少年鸿蒙南向编程挑战赛 （王强）
- 自由讨论linkboy和OpenHarmony的相关话题 （王强）

## Attendees/出席者
- 王强 910360201@qq.com （https://gitee.com/linkboy_crux/linkboy）
- 房隆军
- 黄明龙
- 李凯
- 朱其罡
- 张路
- 王城

## Notes/备注
1. 明确了技术卡点，准备进行解决
2. 探索面向青少年的OpenHarmony推广形式，初步确定比赛参与和协调机制
3. 暂停对比赛的推进，先解决openHarmony的技术问题

## Action items/活动项目
- 完成wifi相关问题的解决，推进上层组件封装


